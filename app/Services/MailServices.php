<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 16.08.2018
 * Time: 0:36
 */

namespace App\Services;
use League\Flysystem\Config;
use Webklex\IMAP\Client;
use \Webklex\IMAP\Folder;
use Illuminate\Support\Facades\Mail;

class MailServices
{

    protected $connect;
    protected $to;
    protected $subject;

    function __construct($email, $password)
    {
        $con = new Client([
            'host'          => 'imap.gmail.com',
            'port'          => 993,
            'encryption'    => 'ssl',
            'validate_cert' => true,
            'username'      => $email,
            'password'      => $password,
            'protocol'      => 'imap'
        ]);
        $this->connect = $con->connect();
    }

    public function getLetter() {

        $folder = new Folder($this->connect, (object)['name' => '{imap.gmail.com:993/imap/novalidate-cert/ssl}INBOX', 'attributes' => 32, 'delimiter' => '.']);
        $aFolder = $folder->getMessages();
        $i = 0;
        foreach ($aFolder as $oMessage) {
            $mas[$i]['date'] = $oMessage->getDate()->toDateTimeString(); // дата
            $mas[$i]['id'] = $oMessage->getUid(); // id
            $mas[$i]['who'] = $oMessage->getFrom()[0]->full; // от кого
            $mas[$i]['subject'] = $oMessage->getSubject(); //тема
            $i++;
        }
        if(isset($mas)) {
            return array_reverse($mas);
        }
            return false;

    }

    public function getSent() {
        $folder = new Folder($this->connect, (object)['name' => '{imap.gmail.com:993/imap/ssl}[Gmail]/&BB4EQgQ,BEAEMAQyBDsENQQ9BD0ESwQ1-', 'attributes' => 32, 'delimiter' => '.']);
        $aSent = $folder->getMessages();
        $i = 0;
        foreach($aSent as $oSent) {
            $mas[$i]['id'] = $oSent->getUid(); //id
            $mas[$i]['who'] = $oSent->to[0]->full;// кому
            $mas[$i]['subject'] = $oSent->getSubject();// Тема
            $mas[$i]['date'] = $oSent->getDate()->toDateTimeString();// Дата
            $i++;
        }
        if(isset($mas)) {
            return array_reverse($mas);
        }
        return false;
    }

    public function deleteIncoming($UID) {
        $folder = new Folder($this->connect, (object)['name' => '{imap.gmail.com:993/imap/novalidate-cert/ssl}INBOX', 'attributes' => 32, 'delimiter' => '.']);
        $folder->getMessage($UID)->delete();
        $this->connect->expunge();
    }

    public function deleteSent($UID) {
        $folder = new Folder($this->connect, (object)['name' => '{imap.gmail.com:993/imap/ssl}[Gmail]/&BB4EQgQ,BEAEMAQyBDsENQQ9BD0ESwQ1-', 'attributes' => 32, 'delimiter' => '.']);
        $a = $folder->getMessages();
        foreach($a as $b) {
            if($UID == $b->getUid()){
                $b->delete();
            }
        }
        $this->connect->expunge();
    }

    public function writeMessage($to, $subject, $text) {
        $this->subject = $subject;
        $this->to = $to;
        Mail::raw($text, function (\Illuminate\Mail\Message $mail) {
           $mail->subject($this->subject);
           $mail->from('testilya993@gmail.com', 'ilya');
           $mail->to($this->to);
        });
    }
}